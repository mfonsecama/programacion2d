#include "file.h"
#pragma warning(disable:4996)

File::File()
{
	fileDescriptor = nullptr;
}

int File::Open(const char * path, const char * mode)
{

	if (!path) { return ERROR_NULL_PATH; }
	if (!mode) { return ERROR_NOT_MODE_SELECTED;  }
	fileDescriptor = fopen(path, mode);
	if (!fileDescriptor) {
		return ERROR_OPENING_FILE;
	}
	return 0;
}

int File::FileSize()
{
	if (!fileDescriptor) { return FILE_NOT_OPENED; }
	fseek(fileDescriptor, 0, SEEK_END);
	int totalFileSize = static_cast<int>(ftell(fileDescriptor));
	rewind(fileDescriptor);
	return totalFileSize;
}

int File::Read(uint8_t * outBuffer, uint32 bufferSize)
{
	if (!fileDescriptor) { return FILE_NOT_OPENED;  }
	if (!outBuffer) { return BUFFER_NOT_INITIALIZED; }
	uint32 readResults;
	readResults = fread(outBuffer, 1, bufferSize, fileDescriptor);
	if (ferror(fileDescriptor)) {
		return ERROR_READING_FILE;
	}
	return static_cast<int>(readResults);
}

int File::Write(const char * inBuffer, uint32 size, uint32 bufferSize)
{
	if (!fileDescriptor) { return FILE_NOT_OPENED; }
	if (!inBuffer) { return BUFFER_NOT_INITIALIZED; }
	int bytesWritten = static_cast<int>(fwrite(inBuffer, static_cast<size_t>(size), static_cast<size_t>(bufferSize), fileDescriptor));
	if (ferror(fileDescriptor)) {
		return ERROR_WRITING_FILE;
	}
	return bytesWritten;
}

char File::GetActualCharacter()
{
	if (!fileDescriptor) { return FILE_NOT_OPENED; }
	return static_cast<char>(fgetc(fileDescriptor));
}

int File::MoveDescriptorToPos(long int offset, int origin)
{
	if (!fileDescriptor) { return FILE_NOT_OPENED; }
	return fseek(fileDescriptor, offset, origin);
}

int File::Close()
{
	if (!fileDescriptor) { return FILE_NOT_OPENED; }
	int ret = fclose(fileDescriptor);
	fileDescriptor = nullptr;
	return ret;
}

File::~File()
{
	if (fileDescriptor) {
		fclose(fileDescriptor);
	}
	fileDescriptor = nullptr;
}
