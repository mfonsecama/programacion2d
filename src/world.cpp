﻿#include "world.h"
#include "sprite.h"
#include "utilities.h"
#include "noxengine.h"

#include <pugixml.hpp>

using namespace pugi;

World::World(float clearRed, float clearGreen, float clearBlue, const ltex_t * back0, const ltex_t * back1, const ltex_t * back2, const ltex_t * back3)
{
	m_ClearColors[0] = clearRed;
	m_ClearColors[1] = clearGreen;
	m_ClearColors[2] = clearBlue;

	m_Backs[0] = back0;
	m_Backs[1] = back1;
	m_Backs[2] = back2;
	m_Backs[3] = back3;

	m_CameraPosition = Vec2(0, 0);

	m_tileHeight = 0;
	m_tileWidth = 0;
	m_tilesHeight = 0;
	m_tilesWidth = 0;

}

float World::getClearRed()
{
	return m_ClearColors[0];
}

float World::getClearGreen()
{
	return m_ClearColors[1];
}

float World::getClearBlue()
{
	return m_ClearColors[2];
}

const ltex_t * World::getBackground(size_t index) const
{
	return m_Backs[index];
}

float World::getScrollRatio(size_t layer) const
{
	return m_ScrollRatios[layer];
}

void World::setScrollRatio(size_t layer, float ratio)
{
	m_ScrollRatios[layer] = ratio;
}

const Vec2 & World::getScrollSpeed(size_t layer) const
{
	return m_ScrollSpeeds[layer];
}

void World::setScrollSpeed(size_t layer, const Vec2 & speed)
{
	m_ScrollSpeeds[layer] = speed;
}

const Vec2 & World::getCameraPosition() const
{
	return m_CameraPosition;
}

void World::setCameraPosition(const Vec2 & pos)
{
	m_CameraPosition = pos;
}

void World::addSprite(Sprite & sprite)
{
	m_Sprites.push_back(&sprite);
}

void World::removeSprite(Sprite & sprite)
{
	m_Sprites.erase(
		std::remove_if(m_Sprites.begin(), m_Sprites.end(), [sprite](Sprite * spr) -> bool { return spr == &sprite; }),
		m_Sprites.end()
	);
}

bool World::loadMap(const char * filename, uint16_t firstColId)
{
	xml_document doc;
	if (!filename) return false;
	xml_parse_result result = doc.load_file(filename);
	if (result) {

		// Analizamos su contenido
		xml_node mapNode = doc.child("map");

		m_tilesWidth = mapNode.attribute("tilewidth").as_uint();
		m_tilesHeight = mapNode.attribute("tileheight").as_uint();

		xml_node tilesetNode = mapNode.child("tileset");

		// Cargamos los atributos del tileset
		uint32_t firstGid = tilesetNode.attribute("firstgid").as_uint();
		int tileWidth = tilesetNode.attribute("tilewidth").as_int();
		int tileHeight = tilesetNode.attribute("tileheight").as_int();

		m_tileHeight = tileHeight;
		m_tileWidth = tileWidth;

		uint32_t tileCount = tilesetNode.attribute("tilecount").as_uint();
		uint32_t columns = tilesetNode.attribute("columns").as_uint();

		xml_node tilesetImageNode = tilesetNode.child("image");
		const std::string tilesSource = tilesetImageNode.attribute("source").as_string();
		uint32_t heightTilesTexture = tilesetImageNode.attribute("height").as_uint();
		uint32_t widthTilesTexture = tilesetImageNode.attribute("width").as_uint();

		/*
		Para cargar la textura, se cargaria con nuestra utilidad de carga de texturas, y los
		separariamos y lo guardariamos en tantas texturas como elementos tengamos en el tileset (std::vector)
		Creamos una estructura con l_tex, m_collider, ��valor de gid - 1?? y es lo que guardamos en el vector
		*/

		std::string path = FileUtils::extractPath(filename);
		ltex_t * textureTiles = TextureUtils::loadTexture(path.append(tilesSource).c_str());
		if (!textureTiles) return false;

		int textureLength = (textureTiles->height * textureTiles->width);
		int rgbaTextureLength = textureLength * 4;
		std::vector<uint8_t> textureBuffer(rgbaTextureLength);

		ltex_getpixels(textureTiles, textureBuffer.data());

		uint8_t rowsNumber = tileCount / columns;

		size_t bufferInit = 0;
		size_t bufferSize = tileWidth * tileHeight * 4;
	
		for (size_t k = 0; k < rowsNumber; k++) {
			for (size_t w = 0; w < columns; w++) {

				std::vector<uint8_t> tempTexture;

				for (size_t j = 0; j < textureTiles->height; j++) {
					for (size_t i = 0; i < textureTiles->width; i++) {
						if (i >= tileWidth * w && i < tileWidth * w + tileWidth) {
							if (j >= tileHeight * k && j < tileHeight * k + tileHeight) {
								int position = i + j * textureTiles->width;
								tempTexture.insert(tempTexture.end(), textureBuffer[position * 4]);
								tempTexture.insert(tempTexture.end(), textureBuffer[position * 4 + 1]);
								tempTexture.insert(tempTexture.end(), textureBuffer[position * 4 + 2]);
								tempTexture.insert(tempTexture.end(), textureBuffer[position * 4 + 3]);
							}
						}
					}
				}

				ltex_t * pointer = ltex_alloc(tileWidth, tileHeight, 0);
				ltex_setpixels(pointer, tempTexture.data());
				m_TilesTextures.push_back(pointer);
			}
		}

		for (pugi::xml_node tileNode = mapNode.child("layer").child("data").child("tile");
			tileNode; tileNode = tileNode.next_sibling("tile")) {
			
			uint32_t gid = tileNode.attribute("gid").as_uint();
			if (gid > 0) {
				Sprite * p = new Sprite(m_TilesTextures.at(gid - 1), 1, 1);
				if (gid >= firstColId) p->setCollisionType(COLLISION_RECT);
				m_WorldTileMap.push_back(WorldTile(gid, p));
			}
			else {
				m_WorldTileMap.push_back(WorldTile(0, nullptr));
			}
				
		}

		TextureUtils::freeTexture(textureTiles);

	}
	else {
		std::cout << result.description() << std::endl;
		return false;
	}
	return true;
}

Vec2 World::getMapSize() const
{
	return Vec2(m_tileWidth * m_tilesWidth, m_tileHeight * m_tilesHeight);
}

bool World::moveSprite(Sprite & sprite, const Vec2 & amount)
{
	bool collisionX = false;
	bool collisionY = false;
	Vec2 defValue;
	Vec2 spritePosition = sprite.getPosition();

	sprite.setPosition(Vec2(spritePosition.x + amount.x, spritePosition.y));
	for (auto & w : m_WorldTileMap) {
		if (w.sprite && w.sprite->getCollisionType() != COLLISION_NONE) {
			if (sprite.collides(*(w.sprite))) {
				collisionX = true;
				break;
			}
		}
	}
	if (collisionX) {
		sprite.setPosition(Vec2(spritePosition.x, spritePosition.y));
	}
	else {
		spritePosition.x += amount.x;
	}
	sprite.setPosition(Vec2(spritePosition.x, spritePosition.y + amount.y));
	for (auto & w : m_WorldTileMap) {
		if (w.sprite && w.sprite->getCollisionType() != COLLISION_NONE) {
			if (sprite.collides(*(w.sprite))) {
				collisionY = true;
				break;
			}
		}
	}

	if (collisionY) {
		sprite.setPosition(Vec2(spritePosition.x, spritePosition.y));
	}

	return collisionY; // Solo se notifica la colision en Y para simular el comportamiento de la practica
}

World::~World()
{
	for (auto & w : m_WorldTileMap) {
		delete w.sprite;
	}

	for (auto & t : m_TilesTextures) {
		TextureUtils::freeTexture(t);
	}
}

void World::update(float deltaTime)
{

	// Se actualiza el valor del scroll automatico en funcion del setSpeed
	for (size_t i = 0; i < m_ScrollSpeeds.size(); i++) {
		m_UpdatedScrollSpeeds[i] = m_UpdatedScrollSpeeds[i] + m_ScrollSpeeds[i] * deltaTime;
	}

	size_t columnCounter = 0;
	size_t rowCounter = 0;

	// Se calculan los collides y posicion para cada coso del mapa
	for (auto & w : m_WorldTileMap) {
		if (w.sprite) {
			w.sprite->setPosition(Vec2(columnCounter * 32, rowCounter));
		}
		//worldTile.sprite->draw();
		//ltex_draw(worldTile.sprite->getTexture(), columnCounter * 32, rowCounter);
		columnCounter++;
		if (columnCounter >= m_tilesWidth) {
			columnCounter = 0;
			rowCounter += m_tileHeight;
		}
	}

	// Llamamos a los metodos update del mundo
	for (auto & sprite : m_Sprites) {
		sprite->update(deltaTime);
	}
}

void World::draw(const Vec2 & screenSize)
{
	lgfx_setblend(BLEND_ALPHA);
	// Borramos el fondo con el color especificado
	lgfx_clearcolorbuffer(m_ClearColors[0], m_ClearColors[1], m_ClearColors[2]);
	// Pintamos los fondos (podemos utilizar ltex_drawrotsized, pintando cada imagen a pantalla completa y calculando las coordenadas de textura en base al scroll y al tamaño de cada imagen)
	// El back0 es el nivel 

	const ltex_t * back0 = m_Backs[0];
	if (back0) {
		float maxHeight = (back0->height - screenSize.y);
		float maxWidth = (back0->width - screenSize.x);
		if (maxHeight < 0) maxHeight = m_tilesHeight * m_tileHeight - screenSize.y;
		if (maxWidth < 0) maxWidth = m_tilesWidth * m_tileHeight - screenSize.x;
		m_CameraPosition.y = MathUtils::clamp(m_CameraPosition.y, 0, maxHeight);
		m_CameraPosition.x = MathUtils::clamp(m_CameraPosition.x, 0, maxWidth);
	}

	auto i = m_Backs.size();
	for (i; i-- > 0; ) {
		const ltex_t * texture = m_Backs[i];
		if (texture) {
			Vec2 textureCoordsTopLeft((m_CameraPosition.x * m_ScrollRatios[i] / texture->width) + (m_UpdatedScrollSpeeds[i].x / texture->width), m_CameraPosition.y * m_ScrollRatios[i] / texture->height + (m_UpdatedScrollSpeeds[i].y / texture->height));
			Vec2 textureCoordsBottomLeft(((m_CameraPosition.x * m_ScrollRatios[i] + screenSize.x) / texture->width) + (m_UpdatedScrollSpeeds[i].x / texture->width), ((m_CameraPosition.y * m_ScrollRatios[i] + screenSize.y) / texture->height) + (m_UpdatedScrollSpeeds[i].y / texture->height));
			ltex_drawrotsized(texture, m_CameraPosition.x, m_CameraPosition.y, 0, 0, 0, screenSize.x, screenSize.y, textureCoordsTopLeft.x, textureCoordsTopLeft.y, textureCoordsBottomLeft.x, textureCoordsBottomLeft.y);
		}
	}

	for (auto & worldTile : m_WorldTileMap) {
		if (worldTile.sprite)
			worldTile.sprite->draw();
	}

	lgfx_setorigin(m_CameraPosition.x, m_CameraPosition.y);
	for (auto & sprite : m_Sprites) {
		sprite->draw();
	}
}
