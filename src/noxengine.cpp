#include "noxengine.h"

#define LITE_GFX_IMPLEMENTATION
#include <litegfx.h>
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#define STB_TRUETYPE_IMPLEMENTATION
#include <stb_truetype.h>

ltex_t * TextureUtils::loadTexture(const char * filename)
{
	int x, y;
	unsigned char * pixels;
	ltex_t * texture;
	if (!filename) { return nullptr; }
	pixels = stbi_load(filename, &x, &y, nullptr, 4);
	if (!pixels) { return nullptr; }
	texture = ltex_alloc(x, y, 0);
	ltex_setpixels(texture, pixels);
	stbi_image_free(pixels);
	return texture;
}

void TextureUtils::freeTexture(ltex_t * texture)
{
	ltex_free(texture);
}

bool ColliderUtils::checkCircleCircle(const Vec2 & pos1, float radius1, const Vec2 & pos2, float radius2)
{
	float diffX = pos1.x - pos2.x;
	float diffY = pos1.y - pos2.y;
	float sumRadius = radius1 + radius2;

	bool ret = ((diffX * diffX + diffY * diffY) < (sumRadius * sumRadius));
	return ret;

}

bool ColliderUtils::checkRectRect(const Vec2 & rectPos1, const Vec2 & rectSize1, const Vec2 & rectPos2, const Vec2 & rectSize2)
{

	Vec2 vertexTopLeftRec1 = rectPos1;

	Vec2 vertexTopLeftRec2 = rectPos2;
	Vec2 vertexTopRightRec2(rectPos2.x + rectSize2.x, rectPos2.y);
	Vec2 vertexBottomLeftRec2(rectPos2.x, rectPos2.y + rectSize2.y);

	if (MathUtils::valInRange(vertexTopLeftRec1.x, vertexTopLeftRec2.x, vertexTopRightRec2.x)) {
		if (MathUtils::valInRange(vertexTopLeftRec1.y, vertexTopLeftRec2.y, vertexBottomLeftRec2.y)) {
			return true;
		}
	}

	Vec2 vertexTopRightRec1(rectPos1.x + rectSize1.x, rectPos1.y);
	if (MathUtils::valInRange(vertexTopRightRec1.x, vertexBottomLeftRec2.x, vertexTopRightRec2.x)) {
		if (MathUtils::valInRange(vertexTopRightRec1.y, vertexTopLeftRec2.y, vertexBottomLeftRec2.y)) {
			return true;
		}
	}

	Vec2 vertexBottomLeftRec1(rectPos1.x, rectPos1.y + rectSize1.y);
	if (MathUtils::valInRange(vertexBottomLeftRec1.x, vertexBottomLeftRec2.x, vertexTopRightRec2.x)) {
		if (MathUtils::valInRange(vertexBottomLeftRec1.y, vertexTopLeftRec2.y, vertexBottomLeftRec2.y)) {
			return true;
		}
	}

	Vec2 vertexBottomRightRec1(rectPos1.x + rectSize1.x, rectPos1.y + rectSize1.y);
	if (MathUtils::valInRange(vertexBottomRightRec1.x, vertexBottomLeftRec2.x, vertexTopRightRec2.x)) {
		if (MathUtils::valInRange(vertexBottomRightRec1.y, vertexTopLeftRec2.y, vertexBottomLeftRec2.y)) {
			return true;
		}
	}

	return false;
}

bool ColliderUtils::checkCircleRect(const Vec2 & circlePos, float circleRadius, const Vec2 & rectPos, const Vec2 & rectSize)
{

	Vec2 vertexTopLeftRec = rectPos;
	Vec2 vertexTopRightRec(rectPos.x + rectSize.x, rectPos.y);
	Vec2 vertexBottomLeftRec(rectPos.x, rectPos.y + rectSize.y);
	Vec2 vertexBottomRightRec(rectPos.x + rectSize.x, rectPos.y + rectSize.y);

	float closestX = MathUtils::clamp(circlePos.x, vertexTopLeftRec.x, vertexBottomRightRec.x);
	float closestY = MathUtils::clamp(circlePos.y, vertexTopLeftRec.y, vertexBottomRightRec.y);

	float diffX = closestX - circlePos.x;
	float diffY = closestY - circlePos.y;

	bool ret = ((diffX * diffX + diffY * diffY) < circleRadius * circleRadius);
	return ret;
}

bool ColliderUtils::checkPixelsPixels(const Vec2 & pixelsPos1, const Vec2 & pixelsSize1, const uint8_t * pixels1, const Vec2 & pixelsPos2, const Vec2 & pixelsSize2, const uint8_t * pixels2)
{

	int pixels1Area = static_cast<int>(pixelsSize1.x * pixelsSize1.y);
	int pixels2Area = static_cast<int>(pixelsSize2.x * pixelsSize2.y);

	if (!checkRectRect(pixelsPos1, pixelsSize1, pixelsPos2, pixelsSize2)) {
		if (!checkRectRect(pixelsPos2, pixelsSize2, pixelsPos1, pixelsSize1)) {
			return false;
		}
	}

	Vec2 topLeft;
	Vec2 topRight;
	Vec2 bottomLeft;
	Vec2 bottomRight;

	calculateIntersectionArea(pixelsPos1, pixelsSize1, pixelsPos2, pixelsSize2, topLeft, topRight, bottomLeft, bottomRight);

	for (int y = topLeft.y; y <= bottomLeft.y; ++y) {
		for (int x = topLeft.x; x <= topRight.x; ++x) {

			int x1 = x - static_cast<int>(pixelsPos1.x);
			int y1 = y - static_cast<int>(pixelsPos1.y);

			int x2 = x - static_cast<int>(pixelsPos2.x);
			int y2 = y - static_cast<int>(pixelsPos2.y);

			int pixel1 = y1 * pixelsSize1.x + x1;
			int pixel2 = x2 + y2 * pixelsSize2.x;

			uint8_t pixel1Val = pixels1[pixel1];
			uint8_t pixel2Val = pixels2[pixel2];

			if (pixel1 >= pixels1Area) {
				break;
			}

			if (pixel2 >= pixels2Area) {
				break;
			}

			if (pixel1Val > ALPHA_PIXEL_COLLISION_THRESHOLD && pixel2Val > ALPHA_PIXEL_COLLISION_THRESHOLD) {
				return true;
			}
		}
	}

	return false;
}

bool ColliderUtils::checkPixelsRect(const Vec2 & pixelsPos, const Vec2 & pixelsSize, const uint8_t * pixels, const Vec2 & rectPos, const Vec2 & rectSize)
{
	size_t rectArea = rectSize.x * rectSize.y;
	std::vector<uint8_t> bufferRect(rectArea);
	bufferRect.assign(rectArea, 255);

	bool ret = checkPixelsPixels(pixelsPos, pixelsSize, pixels, rectPos, rectSize, bufferRect.data());
	return ret;
}

bool ColliderUtils::checkCirclePixels(const Vec2 & circlePos, float circleRadius, const Vec2 & pixelsPos, const Vec2 & pixelsSize, const uint8_t * pixels)
{

	int pixelsArea = static_cast<int>(pixelsSize.x * pixelsSize.y);

	Vec2 boundingBoxCircleSize(circleRadius + circleRadius, circleRadius + circleRadius);
	Vec2 boundingBoxCirclePos(circlePos.x - circleRadius, circlePos.y - circleRadius);

	if (!checkRectRect(boundingBoxCirclePos, boundingBoxCircleSize, pixelsPos, pixelsSize)) {
		if (!checkRectRect(pixelsPos, pixelsSize, boundingBoxCirclePos, boundingBoxCircleSize)) {
			return false;
		}
	}

	Vec2 topLeft;
	Vec2 topRight;
	Vec2 bottomLeft;
	Vec2 bottomRight;

	calculateIntersectionArea(boundingBoxCirclePos, boundingBoxCircleSize, pixelsPos, pixelsSize, topLeft, topRight, bottomLeft, bottomRight);

	for (int y = topLeft.y; y <= bottomLeft.y; ++y) {
		for (int x = topLeft.x; x <= topRight.x; ++x) {

			int x1 = x - static_cast<int>(pixelsPos.x);
			int y1 = y - static_cast<int>(pixelsPos.y);
			int pixel1 = y1 * pixelsSize.x + x1;

			uint8_t pixelVal = pixels[pixel1];

			if (pixel1 >= pixelsArea) {
				break;
			}

			if (pixelVal > ALPHA_PIXEL_COLLISION_THRESHOLD) {

				float diffX = x - circlePos.x;
				float diffY = y - circlePos.y;

				bool ret = ((diffX * diffX + diffY * diffY) < circleRadius * circleRadius);
				if (ret) {
					return ret;
				}
			}
		}
	}

	return false;
}

void ColliderUtils::calculateIntersectionArea(const Vec2 & posRect1, const Vec2 & sizeRect1, const Vec2 & posRect2, const Vec2 & sizeRect2,
	Vec2 & topLeft, Vec2 & topRight, Vec2 & bottomLeft, Vec2 & bottomRight)
{

	Vec2 vertexTopLeftPixel1(posRect1);
	Vec2 vertexTopRightPixel1(posRect1.x + sizeRect1.x, posRect1.y);
	Vec2 vertexBottomLeftPixel1(posRect1.x, posRect1.y + sizeRect1.y);
	Vec2 vertexBottomRightPixel1(posRect1.x + sizeRect1.x, posRect1.y + sizeRect1.y);

	Vec2 vertexTopLeftPixel2(posRect2);
	Vec2 vertexTopRightPixel2(posRect2.x + sizeRect2.x, posRect2.y);
	Vec2 vertexBottomLeftPixel2(posRect2.x, posRect2.y + sizeRect2.y);
	Vec2 vertexBottomRightPixel2(posRect2.x + sizeRect2.x, posRect2.y + sizeRect2.y);

	if (vertexTopLeftPixel1.y < vertexTopLeftPixel2.y) topLeft.y = vertexTopLeftPixel2.y; else topLeft.y = vertexTopLeftPixel1.y;
	if (vertexTopLeftPixel1.x < vertexTopLeftPixel2.x) topLeft.x = vertexTopLeftPixel2.x; else topLeft.x = vertexTopLeftPixel1.x;

	if (vertexTopRightPixel1.y < vertexTopRightPixel2.y) topRight.y = vertexTopRightPixel2.y; else topRight.y = vertexTopRightPixel1.y;
	if (vertexTopRightPixel1.x < vertexTopRightPixel2.x) topRight.x = vertexTopRightPixel1.x; else topRight.x = vertexTopRightPixel2.x;

	if (vertexBottomLeftPixel1.y < vertexBottomLeftPixel2.y) bottomLeft.y = vertexBottomLeftPixel1.y; else bottomLeft.y = vertexBottomLeftPixel2.y;
	if (vertexBottomLeftPixel1.x < vertexBottomLeftPixel2.x) bottomLeft.x = vertexBottomLeftPixel2.x; else bottomLeft.x = vertexBottomLeftPixel1.x;

	if (vertexBottomRightPixel1.y < vertexBottomRightPixel2.y) bottomRight.y = vertexBottomRightPixel1.y; else bottomRight.y = vertexBottomRightPixel2.y;
	if (vertexBottomRightPixel1.x < vertexBottomRightPixel2.x) bottomRight.x = vertexBottomRightPixel1.x; else bottomRight.x = vertexBottomRightPixel2.x;

}