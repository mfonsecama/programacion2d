#pragma once

#include <stdio.h>
#include <vector>

typedef unsigned int uint32;

class File {

private:
	// Variables miembro
	FILE * fileDescriptor;

public:

	enum Error
	{
		ERROR_NULL_PATH = -1,
		ERROR_OPENING_FILE = -2,
		ERROR_READING_FILE = -3,
		ERROR_WRITING_FILE = -4,
		ERROR_CLOSING_FILE = -5,
		ERROR_NOT_MODE_SELECTED = -6,
		FILE_NOT_OPENED = -7,
		BUFFER_NOT_INITIALIZED = -8

	};

	File();

	// Funciones miembro
	int Open(const char * path, const char * mode);
	int FileSize();
	int Read(uint8_t * outBuffer, uint32 bufferSize);
	int Write(const char * inBuffer, uint32 size, uint32 bufferSize);
	char GetActualCharacter();
	int MoveDescriptorToPos(long int offset, int origin);
	int Close();

	~File();

};