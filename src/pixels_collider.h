#pragma once

#include "collider.h"

#include <stdint.h>

class PixelsCollider : public Collider {
private:
	const Vec2 & m_Pos;
	const Vec2 & m_Size;
	const Vec2 & m_Scale;
	const Vec2 & m_Pivot;
	const uint8_t * m_AlphaBuffer;

public:

	PixelsCollider(const Vec2 & pos, const Vec2 & size, const Vec2 & pivot, const Vec2 & scale, const uint8_t * alphaBuffer) : m_Pos(pos), m_Size(size), m_Pivot(pivot), m_Scale(scale), m_AlphaBuffer(alphaBuffer) {};

	virtual bool collides(const Collider & other) const;
	virtual bool collides(const Vec2 & circlePos, float circleRadius) const;
	virtual bool collides(const Vec2 & rectPos, const Vec2 & rectSize) const;
	virtual bool collides(const Vec2 & pixelsPos, const Vec2 & pixelsSize, const uint8_t * pixels) const;

	virtual void draw() const;
};