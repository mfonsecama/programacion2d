#pragma once

#include "font.h"

struct FontWrapper {

	Font * font;
	Vec2 currentPos;
	float velocity;
	float r;
	float g;
	float b;

	void update(float x) {
		currentPos.x = x;
	}
};