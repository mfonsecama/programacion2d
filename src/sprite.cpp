#include "sprite.h"
#include "vec2.h"
#include "pixels_collider.h"
#include "circle_collider.h"
#include "rect_collider.h"

uint8_t * Sprite::calculateAlphaBuffer(const ltex_t * texture, size_t bufferSize, int prevBufferPos)
{
	int textureLength = (texture->height * texture->width);
	int rgbaTextureLength = textureLength * 4;
	std::vector<uint8_t> textureBuffer(rgbaTextureLength);
	uint8_t * alphaBuffer = new uint8_t[bufferSize];

	ltex_getpixels(texture, textureBuffer.data());

	size_t initBuffer = bufferSize * prevBufferPos;
	size_t endBuffer = initBuffer + bufferSize;

	for (size_t i = initBuffer; i < endBuffer; ++i) {
		alphaBuffer[i - initBuffer] = textureBuffer[i * 4 + 3];
	}

	return alphaBuffer;

}

void Sprite::deleteAlphaBuffer()
{
	std::array<uint8_t *, 20>::iterator start = m_AlphaBufferArray.begin();
	std::array<uint8_t *, 20>::iterator end = m_AlphaBufferArray.end();
	for (start; start != end; start++) {
		uint8_t * data = *start;
		if (data) delete data;
	}
}

Sprite::Sprite(const ltex_t * tex, int hFrames, int vFrames)
{
	m_Texture = tex;
	m_HFrames = hFrames;
	m_VFrames = vFrames;

	m_Red = 1.0f;
	m_Green = 1.0f;
	m_Blue = 1.0f;
	m_Alpha = 1.0f;
	m_Angle = 0.0f;

	m_Blend = BLEND_ALPHA;

	m_Scale = Vec2(1.0f, 1.0f);
	m_Fps = 1;
	m_CurrentFrame = 0;

	float width = static_cast<float>(m_Texture->width) / m_HFrames;
	float height = static_cast<float>(m_Texture->height) / m_VFrames;
	m_SizeBase = Vec2(width, height);

}

const ltex_t * Sprite::getTexture() const
{
	return m_Texture;
}

void Sprite::setTexture(const ltex_t * tex)
{
	m_Texture = tex;
	float width = static_cast<float>(m_Texture->width) / m_HFrames;
	float height = static_cast<float>(m_Texture->height) / m_VFrames;
	m_SizeBase = Vec2(width, height);
	size_t totalFrames = m_HFrames * m_VFrames;
	if (m_CollisionType == COLLISION_PIXELS) {
		for (size_t i = 0; i < totalFrames; i++) {
			m_AlphaBufferArray[i] = calculateAlphaBuffer(m_Texture, m_SizeBase.x * m_SizeBase.y, i);
		}
	}
}

lblend_t Sprite::getBlend() const
{
	return m_Blend;
}

void Sprite::setBlend(lblend_t mode)
{
	m_Blend = mode;
}

float Sprite::getRed() const
{
	return m_Red;
}

float Sprite::getGreen() const
{
	return m_Green;
}

float Sprite::getBlue() const
{
	return m_Blue;
}

float Sprite::getAlpha() const
{
	return m_Alpha;
}

void Sprite::setColor(float r, float g, float b, float a)
{
	m_Red = r;
	m_Green = g;
	m_Blue = b;
	m_Alpha = a;
}

const Vec2 & Sprite::getPosition() const
{
	return m_Position;
}

void Sprite::setPosition(const Vec2 & pos)
{
	m_Position = pos;
}

float Sprite::getAngle() const
{
	return m_Angle;
}

void Sprite::setAngle(float angle)
{
	m_Angle = angle;
}

const Vec2 & Sprite::getScale() const
{
	return m_Scale;
}

void Sprite::setScale(const Vec2 & scale)
{
	m_Scale = scale;
}

Vec2 Sprite::getSize() const
{
	return m_SizeBase * m_Scale;
}

const Vec2 & Sprite::getPivot() const
{
	return m_Pivot;
}

void Sprite::setPivot(const Vec2 & pivot)
{
	m_Pivot = pivot;
}

int Sprite::getHframes() const
{
	return m_HFrames;
}

int Sprite::getVframes() const
{
	return m_VFrames;
}

void Sprite::setFrames(int hframes, int vframes)
{
	m_HFrames = hframes;
	m_VFrames = vframes;
}

int Sprite::getFps() const
{
	return m_Fps;
}

void Sprite::setFps(int fps)
{
	m_Fps = fps;
}

float Sprite::getCurrentFrame() const
{
	return m_CurrentFrame;
}

void Sprite::setCurrentFrame(int frame)
{
	m_CurrentFrame = static_cast<float>(frame);
}

const uint8_t * Sprite::getAlphaBuffer() const
{
	return m_AlphaBuffer;
}

void Sprite::setCollisionType(CollisionType type)
{

	float radius = 0;
	size_t totalFrames = 0;
	m_CollisionType = type;
	float xCord = 0;
	float yCord = 0;

	if (m_Collider) delete m_Collider;
	m_Collider = nullptr;

	switch (type)
	{
	case COLLISION_NONE:
		if (m_Collider) delete m_Collider;
		m_Collider = nullptr;
		deleteAlphaBuffer();
		break;
	case COLLISION_CIRCLE:
		m_Collider = new CircleCollider(m_Position, m_SizeBase, m_Pivot, m_Scale);
		break;
	case COLLISION_RECT:
		m_Collider = new RectCollider(m_Position, m_SizeBase, m_Pivot, m_Scale);
		break;
	case COLLISION_PIXELS:
	    totalFrames = m_HFrames * m_VFrames;
		for (size_t i = 0; i < totalFrames; i++) {
			m_AlphaBufferArray[i] = calculateAlphaBuffer(m_Texture, m_SizeBase.x * m_SizeBase.y, i);
		}
		m_Collider = new PixelsCollider(m_Position, m_SizeBase, m_Pivot, m_Scale, m_AlphaBufferArray.at(m_CurrentFrame));
		break;
	default:
		break;
	}
}

CollisionType Sprite::getCollisionType() const
{
	return m_CollisionType;
}

const Collider * Sprite::getCollider() const
{
	return m_Collider;
}

bool Sprite::collides(const Sprite & other) const
{
	if (m_Collider) {
		return m_Collider->collides(*other.getCollider());
	} 
	return false;
}

// TODO: Esto puede colisionar con otro sprite o con un WorldTile

void Sprite::update(float deltaTime)
{
	int maxFrames = m_HFrames * m_VFrames - 1;
	float fPerDelta = m_Fps * deltaTime;
	m_CurrentFrame += fPerDelta;
	if (abs(m_CurrentFrame) > maxFrames && m_Fps > 0) {
		m_CurrentFrame = 0;
	}
	else if (abs(m_CurrentFrame) > maxFrames && m_Fps < 0) {
		m_CurrentFrame = static_cast<float>(maxFrames);
	}
	if (m_HFrames * m_VFrames > 1) {
		if (m_CollisionType == COLLISION_PIXELS)
			m_AlphaBuffer = m_AlphaBufferArray.at(m_CurrentFrame);
	}
}

void Sprite::draw() const
{
	//1. Se calculan las coordenadas UV de textura en funcion del frame
	Vec2 currentSize = getSize();
	float frameX = (static_cast<int>(m_CurrentFrame) % m_HFrames) * currentSize.x;
	float frameY = (static_cast<int>(m_CurrentFrame) / m_HFrames) * currentSize.y;
	Vec2 framePosition(frameX, frameY);
	//2. Propiedades de pintado (color and alpha)
	lgfx_setblend(m_Blend);
	lgfx_setcolor(m_Red, m_Green, m_Blue, m_Alpha);
	//3. Dibujar la textura con ltex_drawrotsized
	float totalTextureWidth = m_Texture->width * m_Scale.x;
	float totalTextureHeight = m_Texture->height * m_Scale.y;
	Vec2 uvTopLeft(framePosition.x / totalTextureWidth, framePosition.y / totalTextureHeight);
	Vec2 uvBottomRight((framePosition.x + currentSize.x) / totalTextureWidth, (framePosition.y + currentSize.y) / totalTextureHeight);
	ltex_drawrotsized(m_Texture, m_Position.x, m_Position.y, m_Angle, m_Pivot.x, m_Pivot.y, currentSize.x, currentSize.y, uvTopLeft.x, uvTopLeft.y, uvBottomRight.x, uvBottomRight.y);

	#ifdef _DEBUG
	if (m_Collider) m_Collider->draw();
	#endif // DEBUG
}

Sprite::~Sprite()
{

	std::array<uint8_t *, 20>::iterator start = m_AlphaBufferArray.begin();
	std::array<uint8_t *, 20>::iterator end = m_AlphaBufferArray.end();
	for (start; start != end; start++) {
		uint8_t * data = *start;
		if (data) delete data;
	}
}
