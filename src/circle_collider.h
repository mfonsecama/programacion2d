#pragma once

#include "collider.h"

#include <stdint.h>

class CircleCollider : public Collider {
private:
	const Vec2 & m_Pos;
	const Vec2 & m_Pivot;
	const Vec2 & m_Size;
	const Vec2 & m_Scale;

public:

	CircleCollider(const Vec2 & pos, const Vec2 & size, const Vec2 & pivot, const Vec2 & scale) : m_Pos(pos), m_Size(size), m_Pivot(pivot), m_Scale(scale) {};

	virtual bool collides(const Collider & other) const;
	virtual bool collides(const Vec2 & circlePos, float circleRadius) const;
	virtual bool collides(const Vec2 & rectPos, const Vec2 & rectSize) const;
	virtual bool collides(const Vec2 & pixelsPos, const Vec2 & pixelsSize, const uint8_t * pixels) const;

	virtual void draw() const;
};