#pragma once

#include "vec2.h"

#include <array>
#include <litegfx.h>
#include <stdint.h>
#include <vector>

class Sprite;
class Collider;

struct WorldTile {
	uint32_t gid;
	Sprite * sprite;

	WorldTile(uint32_t g, Sprite * s) : gid(g), sprite(s) { };
};

class World {

private:

	std::array<float, 3> m_ClearColors = {0, 0, 0};
	std::array<const ltex_t *, 4> m_Backs = { nullptr, nullptr, nullptr, nullptr };
	std::array<float, 4> m_ScrollRatios = { 0, 0, 0, 0 };
	std::array<Vec2, 4> m_ScrollSpeeds = { Vec2(), Vec2(), Vec2(), Vec2() };

	std::array<Vec2, 4> m_UpdatedScrollSpeeds = { Vec2(), Vec2(), Vec2(), Vec2() };
	
	Vec2 m_CameraPosition;

	std::vector<Sprite *> m_Sprites;

	uint32_t m_tilesWidth;
	uint32_t m_tilesHeight;
	uint32_t m_tileWidth;
	uint32_t m_tileHeight;
	std::vector<ltex_t *> m_TilesTextures;
	std::vector<WorldTile> m_WorldTileMap;

public:

	World(float clearRed = 0.15f, float clearGreen = 0.15f, float clearBlue = 0.15f,
		  const ltex_t* back0 = nullptr, const ltex_t* back1 = nullptr,
		  const ltex_t* back2 = nullptr, const ltex_t* back3 = nullptr);

	float getClearRed();
	float getClearGreen();
	float getClearBlue();

	const ltex_t* getBackground(size_t index) const;
	
	float getScrollRatio(size_t layer) const;
	void setScrollRatio(size_t layer, float ratio);

	const Vec2& getScrollSpeed(size_t layer) const;
	void setScrollSpeed(size_t layer, const Vec2& speed);

	const Vec2& getCameraPosition() const;
	void setCameraPosition(const Vec2& pos);

	void addSprite(Sprite& sprite);
	void removeSprite(Sprite& sprite);

	void update(float deltaTime);
	void draw(const Vec2& screenSize);

	bool loadMap(const char* filename, uint16_t firstColId);
	Vec2 getMapSize() const;
	bool moveSprite(Sprite& sprite, const Vec2& amount);

	/* Destruir los sprites de los world tile*/
	~World(); 

};