#pragma once

#include "utilities.h"
#include "vec2.h"

#include <array>
#include <litegfx.h>
#include <vector>

class Collider;

enum CollisionType {
	COLLISION_NONE,
	COLLISION_CIRCLE,
	COLLISION_RECT,
	COLLISION_PIXELS
};

class Sprite {
private:

	const ltex_t * m_Texture; 
	const uint8_t * m_AlphaBuffer;
	const Collider * m_Collider;

	std::array<uint8_t *, 20> m_AlphaBufferArray;

	lblend_t m_Blend;
	int m_HFrames;
	int m_VFrames;

	Vec2 m_SizeBase;

	float m_Red;
	float m_Green;
	float m_Blue;
	float m_Alpha;

	Vec2 m_Position;
	float m_Angle;
	Vec2 m_Scale;
	Vec2 m_Pivot;

	int m_Fps;
	float m_CurrentFrame;

	size_t m_AlphaBufferSize;

	CollisionType m_CollisionType;

protected:

	static uint8_t * calculateAlphaBuffer(const ltex_t * texture, size_t bufferSize, int prevBufferPos);
	void deleteAlphaBuffer();

public:

	Sprite(const ltex_t * tex, int hFrames = 1, int vFrames = 1);

	const ltex_t* getTexture() const;
	void setTexture(const ltex_t* tex);

	lblend_t getBlend() const;
	void setBlend(lblend_t mode);

	float getRed() const;
	float getGreen() const;
	float getBlue() const;
	float getAlpha() const;
	void setColor(float r, float g, float b, float a);

	const Vec2 & getPosition() const;
	void setPosition(const Vec2 & pos);

	float getAngle() const;
	void setAngle(float angle);

	const Vec2 & getScale() const;
	void setScale(const Vec2 & scale);

	// Tama�o de un frame multiplicado por la escala
	Vec2 getSize() const;

	// Este valor se pasa a ltex_drawrotsized en el pintado
	// para indicar el pivote de rotaci�n
	const Vec2 & getPivot() const;
	void setPivot(const Vec2 & pivot);

	int getHframes() const;
	int getVframes() const;
	void setFrames(int hframes, int vframes);

	// Veces por segundo que se cambia el frame de animaci�n
	int getFps() const;
	void setFps(int fps);

	// Frame actual de animaci�n
	float getCurrentFrame() const;
	void setCurrentFrame(int frame);

	const uint8_t * getAlphaBuffer() const;

	void setCollisionType(CollisionType type);
	CollisionType getCollisionType() const;

	const Collider * getCollider() const;
	bool collides(const Sprite & other) const;

	void update(float deltaTime);
	void draw() const;

	~Sprite();
};