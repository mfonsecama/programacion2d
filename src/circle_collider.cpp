#include "circle_collider.h"
#include "noxengine.h"

/*CircleCollider::CircleCollider(const Vec2 & center, const float & radius, const Vec2 & pivot)
{
	m_Pos.x = center.x - radius * 2 * pivot.x;
	m_Pos.y = center.y - radius * 2 * pivot.y;
	m_Radius = radius;
}*/

bool CircleCollider::collides(const Collider & other) const
{
	Vec2 scaled(m_Size * m_Scale);
	float radius = m_Size.x / 2 * m_Scale.x;
	Vec2 noPivotedPos(m_Pos.x - radius * 2 * m_Pivot.x, m_Pos.y - radius * 2 * m_Pivot.y);
	return other.collides(Vec2(noPivotedPos.x + radius, noPivotedPos.y + radius), radius);
}

bool CircleCollider::collides(const Vec2 & circlePos, float circleRadius) const
{
	Vec2 scaled(m_Size * m_Scale);
	float radius = m_Size.x / 2 * m_Scale.x;
	// Se calcula el centro
	Vec2 noPivotedPos(m_Pos.x - radius * 2 * m_Pivot.x, m_Pos.y - radius * 2 * m_Pivot.y);
	return ColliderUtils::checkCircleCircle(Vec2(noPivotedPos.x + radius, noPivotedPos.y + radius), radius, circlePos, circleRadius);
}

bool CircleCollider::collides(const Vec2 & rectPos, const Vec2 & rectSize) const
{
	Vec2 scaled(m_Size * m_Scale);
	float radius = (scaled.x > scaled.y ? scaled.x / 2 : scaled.y / 2);
	Vec2 noPivotedPos(m_Pos.x - radius * 2 * m_Pivot.x, m_Pos.y - radius * 2 * m_Pivot.y);
	return ColliderUtils::checkCircleRect(Vec2(noPivotedPos.x + radius, noPivotedPos.y + radius), radius, rectPos, rectSize);
}

bool CircleCollider::collides(const Vec2 & pixelsPos, const Vec2 & pixelsSize, const uint8_t * pixels) const
{
	Vec2 scaled(m_Size * m_Scale);
	float radius = (scaled.x > scaled.y ? scaled.x / 2 : scaled.y / 2);
	Vec2 noPivotedPos(m_Pos.x - radius * 2 * m_Pivot.x, m_Pos.y - radius * 2 * m_Pivot.y);

	return ColliderUtils::checkCirclePixels(Vec2(noPivotedPos.x + radius, noPivotedPos.y + radius), radius, pixelsPos, pixelsSize, pixels);
}

void CircleCollider::draw() const
{
	Vec2 scaled(m_Size * m_Scale);
	float radius = (scaled.x > scaled.y ? scaled.x / 2 : scaled.y / 2);
	Vec2 noPivotedPos(m_Pos.x - radius * 2 * m_Pivot.x, m_Pos.y - radius * 2 * m_Pivot.y);
	lgfx_setcolor(0.988, 0.580, 0.231, 0.3);
	lgfx_drawoval(noPivotedPos.x, noPivotedPos.y, radius * 2, radius * 2);
}
