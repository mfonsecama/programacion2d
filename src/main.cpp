#ifdef _MSC_VER
//#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
#endif

#include "noxengine.h"
#include "font_wrapper.h"

#include <glfw3.h>

#include <random>
#include <list>

using namespace std;

int main() {
	// Inicializamos GLFW
	if (!glfwInit()) {
		cout << "Error: No se ha podido inicializar GLFW" << endl;
		return -1;
	}
	atexit(glfwTerminate);

	// Creamos la ventana
	glfwWindowHint(GLFW_RESIZABLE, false);
	GLFWwindow* window = glfwCreateWindow(800, 600, "Programacion 2D", nullptr, nullptr);
	if (!window) {
		cout << "Error: No se ha podido crear la ventana" << endl;
		return -1;
	}

	// Activamos contexto de OpenGL
	glfwMakeContextCurrent(window);

	// Inicializamos LiteGFX
	lgfx_setup2d(800, 600);

	// Inicializamos variables bucle principal

	ltex_t * backgroundTexture = TextureUtils::loadTexture("data/background.png");
	if (!backgroundTexture) {
		cout << "Error: No se ha podido cargar la textura" << endl;
		return -1;
	}

	ltex_t * playerRun = TextureUtils::loadTexture("data/run.png");
	if (!playerRun) {
		cout << "Error: No se ha podido cargar la textura" << endl;
		return -1;
	}

	ltex_t * playerIdle = TextureUtils::loadTexture("data/idle.png");
	if (!playerIdle) {
		cout << "Error: No se ha podido cargar la textura" << endl;
		return -1;
	}


	World world(0.003f, 0.007f, 0.098f, backgroundTexture);
	world.loadMap("data/map.tmx", 6);

	double lastTime = glfwGetTime();

	Sprite sIdle(playerIdle);
	sIdle.setCollisionType(COLLISION_RECT);
	sIdle.setPivot(Vec2(0.5, 0.5));
	sIdle.setPosition(Vec2(60, 860));

	Sprite sRun(playerRun, 6, 1);
	sRun.setCollisionType(COLLISION_RECT);
	sRun.setPosition(Vec2(60, 860));
	sRun.setPivot(Vec2(0.5, 0.5));
	sRun.setFps(6);

	bool canJump = false;

	float velocityY = 0.0f;
	float positionX = 0.0f;
	float positionY = 0.0f;

	float gravity = 450.0f;
	float speed = 120.0f;

	Sprite * s = &sIdle;

	// Bucle principal
	while (!glfwWindowShouldClose(window) && !glfwGetKey(window, GLFW_KEY_ESCAPE)) {
		// Actualizamos delta
		float deltaTime = static_cast<float>(glfwGetTime() - lastTime);
		lastTime = glfwGetTime();

		// Actualizamos tama�o de ventana
		int screenWidth, screenHeight;
		glfwGetWindowSize(window, &screenWidth, &screenHeight);
		lgfx_setviewport(0, 0, screenWidth, screenHeight);

		// Posicion del cursor
		double mouseX, mouseY;
		glfwGetCursorPos(window, &mouseX, &mouseY);

		int leftKey = glfwGetKey(window, GLFW_KEY_LEFT);
		int rightKey = glfwGetKey(window, GLFW_KEY_RIGHT);
		int spaceKey = glfwGetKey(window, GLFW_KEY_SPACE);

		s = &sIdle;

		float pressFactor = 0;
		if (rightKey == GLFW_PRESS) {
			s = &sRun;
			s->setScale(Vec2(1, 1));
			pressFactor = 1;
		}
		if (leftKey == GLFW_PRESS) {
			s = &sRun;
			s->setScale(Vec2(-1, 1));
			pressFactor = -1;
		}
		if (spaceKey == GLFW_PRESS) {
			if (canJump)
				velocityY = -250.0f;
		}

		positionX = pressFactor * speed * deltaTime;
		positionY = velocityY * deltaTime;
		velocityY += gravity * deltaTime;
		velocityY = MathUtils::clamp(velocityY, -270, 270); // Ajustamos la velocidad para que no aumente hasta el infinito

		// Se actualiza la posicion de ambos sprites
		bool collision = world.moveSprite(*s, Vec2(positionX, positionY));

		sRun.setPosition(s->getPosition());
		sIdle.setPosition(s->getPosition());
		sIdle.setScale(s->getScale());

		if (collision) canJump = true;
		else canJump = false;

		world.setCameraPosition(Vec2(s->getPosition().x - screenWidth / 2, s->getPosition().y - screenHeight / 2));
		world.update(deltaTime);

		world.draw(Vec2(static_cast<float>(screenWidth), static_cast<float>(screenHeight)));
		s->update(deltaTime);
		s->draw();

		// Actualizamos ventana y eventos
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	TextureUtils::freeTexture(backgroundTexture);
	TextureUtils::freeTexture(playerRun);
	TextureUtils::freeTexture(playerIdle);

    return 0;
}
