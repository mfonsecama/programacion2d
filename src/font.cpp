#include "font.h"
#include "file.h"
#include "vec2.h"

#include <vector>


Font::Font(ltex_t * texture, stbtt_bakedchar * charData, size_t charDataSize, float height)
{
	m_Texture = texture;
	m_CharData.assign(charData, charData + charDataSize);
	m_Height = height;
}


Font * Font::load(const char * filename, float height)
{
	
	File fd = File();
	int ok = fd.Open(filename, "rb+");
	if (ok != 0) { return nullptr; }
	int fileSize = fd.FileSize();
	std::vector<uint8_t> fileData(fileSize);
	int bytesReaded = fd.Read(fileData.data(), fileSize);
	if (!bytesReaded) { return nullptr; }
	fd.Close();

	size_t size = 1;
	std::vector<uint8_t> pixels(size * size);
	std::vector<stbtt_bakedchar> charData(128);

	while (stbtt_BakeFontBitmap(fileData.data(), 0, height, pixels.data(), size, size, 0, 128, charData.data()) <= 0) {
		size *= 2;
		pixels.resize(size * size);
	}
	size_t bufferSize = pixels.size();
	std::vector<uint8_t> colorBuffer(bufferSize * 4);
	colorBuffer.assign(bufferSize * 4, 255);
	std::vector<uint8_t>::iterator i = colorBuffer.begin();
	std::vector<uint8_t>::iterator end = colorBuffer.end();
	std::vector<uint8_t>::iterator pixelsIt = pixels.begin();
	size_t counter = 0;
	while (i != end) {
		counter++;
		if (!(counter % 4)) { *i = *pixelsIt; pixelsIt++; }
		i++;
	}
	ltex_t * texture = ltex_alloc(size, size, 0);
	ltex_setpixels(texture, colorBuffer.data());

	Font * font = new Font(texture, charData.data(), 128, height);

	return font;
}

float Font::getHeight() const
{
	return m_Height;
}

Vec2 Font::getTextSize(const char * text) const
{
	float xPos = 0, yPos = 0;
	std::vector<stbtt_aligned_quad> q(1);
	int len = strlen(text);
	float textHeight = 0;
	for (int i = 0; i < len; i++)
	{
		char chr = text[i];
		stbtt_GetBakedQuad(m_CharData.data(), m_Texture->width, m_Texture->height, chr,
			&xPos, &yPos, q.data(), true);
		float currentDiference = q.data()->y1 - q.data()->y0;
		if (textHeight < currentDiference) { textHeight = currentDiference; }
	}
	return Vec2(xPos, textHeight);
}

void Font::draw(const char * text, const Vec2 & pos) const
{
	float xPos = pos.x;
	float yPos = pos.y;
	Vec2 currentPos(xPos, yPos);
	std::vector<stbtt_aligned_quad> q(1);
	int len = strlen(text);
	for (int i = 0; i < len; i++)
	{
		char chr = text[i];
		stbtt_GetBakedQuad(m_CharData.data(), m_Texture->width, m_Texture->height, chr,
			&xPos, &yPos, q.data(), true);
		ltex_drawrotsized(m_Texture, currentPos.x, currentPos.y, 0, 0, 1, q.data()->x1 - q.data()->x0, q.data()->y1 - q.data()->y0, q.data()->s0, q.data()->t0, q.data()->s1, q.data()->t1);
		currentPos.x = xPos;
		currentPos.y = yPos;
	}
}

Font::~Font()
{
	ltex_free(m_Texture);
}
