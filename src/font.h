#pragma once

#include <litegfx.h>
#include <stb_truetype.h>
#include <vector>

struct Vec2;

class Font {

private:
	ltex_t * m_Texture;
	std::vector<stbtt_bakedchar> m_CharData;
	float m_Height;

protected:
	Font(ltex_t * texture, stbtt_bakedchar * charData, size_t charDataSize, float height);

public:
	static Font * load(const char * filename, float height);
	float getHeight() const;
	Vec2 getTextSize(const char * text) const; // El valor mas grande
	void draw(const char * text, const Vec2 & pos) const;

	~Font();
};

