#pragma once

#include "collider.h"
#include "circle_collider.h"
#include "rect_collider.h"
#include "pixels_collider.h"
#include "font.h"
#include "sprite.h"
#include "utilities.h"
#include "vec2.h"
#include "world.h"

#include <litegfx.h>
#include <pugixml.hpp>

#define ALPHA_PIXEL_COLLISION_THRESHOLD 172

namespace TextureUtils {
	ltex_t * loadTexture(const char * filename);

	void freeTexture(ltex_t * texture);
};

namespace ColliderUtils {
	bool checkCircleCircle(const Vec2 & pos1, float radius1, const Vec2 & pos2, float radius2);
	bool checkRectRect(const Vec2 & rectPos1, const Vec2 & rectSize1, const Vec2 & rectPos2, const Vec2 & rectSize2);
	bool checkCircleRect(const Vec2 & circlePos, float circleRadius, const Vec2 & rectPos, const Vec2& rectSize);
	bool checkPixelsPixels(const Vec2 & pixelsPos1, const Vec2 & pixelsSize1, const uint8_t* pixels1, const Vec2 & pixelsPos2, const Vec2 & pixelsSize2, const uint8_t* pixels2);
	bool checkPixelsRect(const Vec2 & pixelsPos, const Vec2 & pixelsSize, const uint8_t* pixels, const Vec2 & rectPos, const Vec2 & rectSize);
	bool checkCirclePixels(const Vec2 & circlePos, float circleRadius, const Vec2 & pixelsPos, const Vec2 & pixelsSize, const uint8_t* pixels);

	void calculateIntersectionArea(const Vec2 & posRect1, const Vec2 & sizeRect1, const Vec2 & posRect2, const Vec2 & sizeRect2, Vec2 & topLeft, Vec2 & topRight, Vec2 & bottomLeft, Vec2 & bottomRight);

}