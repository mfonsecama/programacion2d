#pragma once

#include <sstream>
#include <iostream>
#include <string>
#include <cmath>


namespace FileUtils {

	inline std::string extractPath(const std::string& filename) {
		std::string filenameCopy = filename;
		while (filenameCopy.find("\\") != std::string::npos) {
			filenameCopy.replace(filenameCopy.find("\\"), 1, "/");
		}
		filenameCopy = filenameCopy.substr(0, filenameCopy.rfind('/'));
		if (filenameCopy.size() > 0) filenameCopy += "/";
		return filenameCopy;
	}

}

namespace StringUtils {

	template <typename T>
	std::string stringFromNumber(T val) {
		std::ostringstream stream;
		stream << std::fixed << val;
		return stream.str();
	}
}

namespace MathUtils {

	inline float clamp(float x, float min, float max) {
		return x < min ? min : (x > max ? max : x);
	}

	inline bool valInRange(float val, float min, float max) {
		return (val >= min && val <= max);
	}

}

namespace ContainerUtils {

	template <typename I>
	I randomElement(I begin, I end)
	{
		const unsigned long n = std::distance(begin, end);
		const unsigned long divisor = (RAND_MAX + 1) / n;

		unsigned long k;
		do { k = std::rand() / divisor; } while (k >= n);

		std::advance(begin, k);
		return begin;
	}
}