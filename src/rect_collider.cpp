#include "rect_collider.h"
#include "noxengine.h"


bool RectCollider::collides(const Collider & other) const
{
	Vec2 scaled(m_Size * m_Scale);
	Vec2 position(m_Pos.x - scaled.x * m_Pivot.x, m_Pos.y - scaled.y * m_Pivot.y);

	return other.collides(position, scaled);
}

bool RectCollider::collides(const Vec2 & circlePos, float circleRadius) const
{
	Vec2 scaled(m_Size * m_Scale);
	Vec2 position(m_Pos.x - scaled.x * m_Pivot.x, m_Pos.y - scaled.y * m_Pivot.y);

	return ColliderUtils::checkCircleRect(circlePos, circleRadius, position, scaled);
}

bool RectCollider::collides(const Vec2 & rectPos, const Vec2 & rectSize) const
{
	Vec2 scaled(m_Size * m_Scale);
	Vec2 position(m_Pos.x - scaled.x * m_Pivot.x, m_Pos.y - scaled.y * m_Pivot.y);

	return ColliderUtils::checkRectRect(rectPos, rectSize, position, scaled);
}

bool RectCollider::collides(const Vec2 & pixelsPos, const Vec2 & pixelsSize, const uint8_t * pixels) const
{
	Vec2 scaled(m_Size * m_Scale);
	Vec2 position(m_Pos.x - scaled.x * m_Pivot.x, m_Pos.y - scaled.y * m_Pivot.y);

	return ColliderUtils::checkPixelsRect(pixelsPos, pixelsSize, pixels, position, scaled);
}

void RectCollider::draw() const
{
	Vec2 scaled(m_Size * m_Scale);
	Vec2 position(m_Pos.x - scaled.x * m_Pivot.x, m_Pos.y - scaled.y * m_Pivot.y);
	lgfx_setcolor(0.988f, 0.282f, 0.231f, 0.3f);
	lgfx_drawrect(position.x, position.y, scaled.x, scaled.y);
}
