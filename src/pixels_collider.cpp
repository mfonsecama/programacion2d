#include "pixels_collider.h"
#include "noxengine.h"


bool PixelsCollider::collides(const Collider & other) const
{
	Vec2 scaled(m_Size * m_Scale);
	Vec2 position(m_Pos.x - scaled.x * m_Pivot.x, m_Pos.y - scaled.y * m_Pivot.y);
	return other.collides(position, scaled, m_AlphaBuffer);
}

bool PixelsCollider::collides(const Vec2 & circlePos, float circleRadius) const
{
	Vec2 scaled(m_Size * m_Scale);
	Vec2 position(m_Pos.x - scaled.x * m_Pivot.x, m_Pos.y - scaled.y * m_Pivot.y);
	return ColliderUtils::checkCirclePixels(circlePos, circleRadius, position, scaled, m_AlphaBuffer);
}

bool PixelsCollider::collides(const Vec2 & rectPos, const Vec2 & rectSize) const
{
	Vec2 scaled(m_Size * m_Scale);
	Vec2 position(m_Pos.x - scaled.x * m_Pivot.x, m_Pos.y - scaled.y * m_Pivot.y);
	bool ret = ColliderUtils::checkPixelsRect(position, scaled, m_AlphaBuffer, rectPos, rectSize);
	return ret;
}

bool PixelsCollider::collides(const Vec2 & pixelsPos, const Vec2 & pixelsSize, const uint8_t * pixels) const
{
	Vec2 scaled(m_Size * m_Scale);
	Vec2 position(m_Pos.x - scaled.x * m_Pivot.x, m_Pos.y - scaled.y * m_Pivot.y);
	return ColliderUtils::checkPixelsPixels(position, scaled, m_AlphaBuffer, pixelsPos, pixelsSize, pixels);
}

void PixelsCollider::draw() const
{
	Vec2 scaled(m_Size * m_Scale);
	Vec2 position(m_Pos.x - scaled.x * m_Pivot.x, m_Pos.y - scaled.y * m_Pivot.y);
	lgfx_setcolor(0.231f, 0.403f, 0.988f, 0.3f);
	lgfx_drawrect(position.x, position.y, scaled.x, scaled.y);
}
